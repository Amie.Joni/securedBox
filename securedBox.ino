// -----------------------------------------------------------------------------
// In this project i made a securedBox
// Using : arduino, 4x4 keypad,servo motor
// -----------------------------------------------------------------------------

#include <Keypad.h>
#include <Servo.h>

const byte bip = 2;//buzzer
const byte CODELENGTH = 5;//password length
const byte ROWS = 4; //four rows
const byte COLS = 4; //four columns
//define the symbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
byte rowPins[ROWS] = {5, 6, 7, 8}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {9, 10, 11, 12}; //connect to the column pinouts of the keypad

//initialize an instance of class NewKeypad
Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

//servo who control the cadna
Servo myservo;

void setup(){
  Serial.begin(9600);//serial communication

  myservo.attach(13);//servo signal
  pinMode(bip,OUTPUT);//buzzer as output

  myservo.write(100);//the close state
}

//var
char storedcode[CODELENGTH] = {'1','5','9','6','3'};//box password
char guestcode[CODELENGTH];//guest temporary password
int charpointer=0;//point to every character in the guest password array
bool starttyping = false;//to know the begining of the password
int compteur = 0;//to save only 5 charactere in password length

void loop(){

  if(gotCode()){//if we have the guest code
      switch(compareCode()){//we analyse the the code
        case 'e':  boxCadna(1);//guestcode = storedcode
        break;
        case 'c':  boxCadna(0);//guestcode != storedcode
        break;
        }
  }

}

void boxCadna(int cmd){//open or close the cadna
  if(cmd == 1){
    Serial.println("open box");
      myservo.write(50);
      delay(100);
    }
  else{
    Serial.println("close box");
      myservo.write(100);
    }
  }

char compareCode(){//compare code and return result
  int flag = 0;

  for(int j=0;j<CODELENGTH;j++)
    if(guestcode[j] != '0'){ flag = 1; }
  if(flag == 0) { Serial.println("code close"); return 'c'; }

  flag = 0;
  for(int j=0;j<CODELENGTH;j++)
    if(storedcode[j] != guestcode[j]){ flag = 1; }
  if(flag == 0) {Serial.println("code equal"); return 'e';}

  }

bool gotCode(){//getting code input

  char customKey = customKeypad.getKey();//getting the char

  if(starttyping == true){
    if(customKey){
      if(customKey != '*' && customKey != '#'){//a char was typed
        if(compteur < 5){
          guestcode[charpointer++] = customKey;
          bipBip(300);
          compteur++;
          Serial.println(customKey);
        }else{
          compteur = 0;
          charpointer = 0;
          bipBip(200);
          bipBip(200);
        }
      }
    }
  }

if(customKey == '*'){//the user will start typing the code
  charpointer = 0;starttyping = true;
  Serial.println("Typing start");
  }

if(customKey == '#'){//the user will end typing the code
  starttyping = false;
  compteur = 0;
  charpointer = 0;
  Serial.println("Typing stop");
  Serial.println(guestcode);
  return true;
  }

  return false;
}

void bipBip(int dd){//bip the buzzer
  digitalWrite(bip,HIGH);
  delay(dd);
  digitalWrite(bip,LOW);
  delay(dd);
}
